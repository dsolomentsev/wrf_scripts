#!/bin/bash
# Clean the outdated info and/or backup modeling results

# Cleaning parameters
oldFiles=5
veryOldFiles=30

# Direcories
nwpOutDir=/SysBackup/wrf/data/OUTPUT/NWP/
chemOutDir=/SysBackup/wrf/data/OUTPUT/CHEM/
gfsDataDir=/SysBackup/wrf/data/gfs/
forecastsBackupDirNWP=/srv/forecasts_backup/NWP/
forecastsBackupDirCHEM=/srv/forecasts_backup/CHEM/

# Current date and time
cYear=`date +%Y`
cMonth=`date +%m`

# Clean the gfs data
echo Cleaning gfs data from:
find $gfsDataDir/$cYear.$cMonth/ -type d -mtime +$oldFiles
rm -rf `find $gfsDataDir/$cYear.$cMonth/ -type d -mtime +$oldFiles`

# Backup modeling results
echo Backup NWP modeling data:
find $nwpOutDir -type f -mtime +$oldFiles
mv `find $nwpOutDir -type f -mtime +$oldFiles` $forecastsBackupDirNWP
#mv `find $chemOutDir -type f -mtime +$oldFiles` $forecastsBackupDirCHEM

# Clean very old backups
echo Deleting very old backups:
find $forecastsBackupDirNWP -type f -mtime +$veryOldFiles
rm -f `find $forecastsBackupDirNWP -type f -mtime +$veryOldFiles`
