#! /bin/bash
# This script runs WRF model from the current moment for some time ahead
# 18-01-2013

echo Start at `date`
echo Warming up...
cd /home/dmitry/Scripts/

#=======================================================================
# Paths and constants

modelDelayInDays=0
forecastHorizonInHr=18
forecastTimeResolutionInHr=3
wrfRegularDir=/home/dmitry/WRFV3/test/chem/
wpsDir=/home/dmitry/WPS/
dataDir=/home/dmitry/data/gfs/
geogDir=/home/dmitry/data/geog/

#=======================================================================
# Time calculation

cYear=`date --date="$modelDelayInDays days ago" +%Y`
cMonth=`date --date="$modelDelayInDays days ago" +%m`
cDay=`date --date="$modelDelayInDays days ago" +%d`
cHour="00" #`date --date="$modelDelayInDays days ago" +%H`
cMin="00"
underscore="_"
logFileStamp=$cYear'_'$cMonth'_'$cDay.log

nextYear=`date --date="$cYear-$cMonth-$cDay++$forecastHorizonInHr hours" +%Y`
nextMonth=`date --date="$cYear-$cMonth-$cDay++$forecastHorizonInHr hours" +%m`
nextDay=`date --date="$cYear-$cMonth-$cDay++$forecastHorizonInHr hours" +%d`
nextHour=`date --date="$cYear-$cMonth-$cDay+$forecastHorizonInHr hours" +%H`
nextMin="00"

#=======================================================================
# Download the GRIB2 GFS forecast files
# HTTP link looks like this:
# http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/gfs.2014040800/

echo Downloading GRIB-2 files...
# Assign the directory for GRIB1 files storage on RAID. Create the directory if it doesn't exist.
gribFilesDir=$dataDir/$cYear.$cMonth/$cDay/
if [ ! -d "$gribFilesDir" ]; then
    mkdir -p $gribFilesDir
fi 

# Get GRIB2 files for the current date and move them to the data dir
declare -a hourArray=( $( seq 0 $forecastTimeResolutionInHr $forecastHorizonInHr ) )
for hr in ${hourArray[@]}
do
	if [ "$hr" -lt "10" ]
	then
	   hr="0"$hr
	fi

	echo Downloading GRIB-2 files hour= $hr...
	currFileName="gfs.t00z.pgrb2f"$hr
	wget "http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/gfs."$cYear$cMonth$cDay"00/"$currFileName > $dataDir/log/wget_$logFileStamp 2>&1 
	mv $currFileName $gribFilesDir/$currFileName
done 

#=======================================================================
# Run WPS

echo Starting WPS system...

# Go to the WPS directory
cd $wpsDir

# Create a new namelist.wps file for WPS run
cat << End_Of_Namelist | sed -e 's/#.*//; s/  *$//' > $wpsDir/namelist.wps
&share
 wrf_core = 'ARW',
 max_dom = 1,
 start_date = '$cYear-$cMonth-$cDay$underscore$cHour:00:00','$cYear-$cMonth-$cDay$underscore$cHour:00:00',
 end_date   = '$nextYear-$nextMonth-$nextDay$underscore$nextHour:00:00','$nextYear-$nextMonth-$nextDay$underscore$cHour:00:00',
 interval_seconds = 10800,
 io_form_geogrid = 2,
/

&geogrid
 parent_id         =   1,   1,
 parent_grid_ratio =   1,   3,
 i_parent_start    =   1,  31,
 j_parent_start    =   1,  17,
 e_we              =  200, 112,
 e_sn              =  200,  97,
 geog_data_res     = '2m','2m',
 dx = 0.009,
 dy = 0.009,
 map_proj = 'lat-lon',
 ref_lat   =  54.859,
 ref_lon   =  36.71,
 ref_x     =  1.0,
 ref_y     =  1.0,
 truelat1  =  30.0,
 truelat2  =  60.0,
 stand_lon = -98.0,
 geog_data_path = '$geogDir'
/

&ungrib
 out_format = 'WPS',
 prefix = 'FILE',
/

&metgrid
 fg_name = 'FILE'
 io_form_metgrid = 2, 
/
End_Of_Namelist

echo Running geogrid...

# Run geogrid
./geogrid.exe > log/geogrid_$logFileStamp 2>&1

echo Running ungrib...

# Run ungrib
ln -s ungrib/Variable_Tables/Vtable.GFS Vtable
./link_grib.csh $gribFilesDir/"gfs.t00z.pgrb2f"*
./ungrib.exe > log/ungrib_$logFileStamp 2>&1

echo Running metgrid...

# Run metgrid
./metgrid.exe > log/metgrid_$logFileStamp 2>&1

# Link the results to the regular running directory
ln -s $wpsDir/geo_em.d0* $wrfRegularDir
ln -s $wpsDir/met_em.d0* $wrfRegularDir

#=======================================================================
# Run WRF preprocessor and then the model

echo Starting WRF initialization...

cd $wrfRegularDir

# Edit the namelist.input file for the current time
cat << End_Of_Namelist | sed -e 's/#.*//; s/  *$//' > $wrfRegularDir/namelist.input
&time_control
 run_days                            = 1,
 run_hours                           = 0,
 run_minutes                         = 0,
 run_seconds                         = 0,
 start_year                          = $cYear, 2000, 2000,
 start_month                         = $cMonth,   01,   01,
 start_day                           = $cDay,   24,   24,
 start_hour                          = $cHour,   12,   12,
 start_minute                        = 00,   00,   00,
 start_second                        = 00,   00,   00,
 end_year                            = $nextYear, 2000, 2000,
 end_month                           = $nextMonth,   01,   01,
 end_day                             = $nextDay,   25,   25,
 end_hour                            = $nextHour,   12,   12,
 end_minute                          = 00,   00,   00,
 end_second                          = 00,   00,   00,
 interval_seconds                    = 10800,
 input_from_file                     = .true.,.true.,.true.,
 history_interval                    = 10,  60,   60,
 frames_per_outfile                  = 1000, 1000, 1000,
 restart                             = .false.,
 restart_interval                    = 5000,
 io_form_history                     = 2
 io_form_restart                     = 2
 io_form_input                       = 2
 io_form_boundary                    = 2
 debug_level                         = 0
 /

 &domains
 time_step                           = 3,
 time_step_fract_num                 = 0,
 time_step_fract_den                 = 1,
 max_dom                             = 1,
 e_we                                = 200,    112,   94,
 e_sn                                = 200,    97,    91,
 e_vert                              = 28,    28,    28,
 p_top_requested                     = 9950,
 num_metgrid_levels                  = 27,
 num_metgrid_soil_levels             = 4,
 dx                                  = 1000.597, 10000,  3333.33,
 dy                                  = 1000.597, 10000,  3333.33,
 grid_id                             = 1,     2,     3,
 parent_id                           = 0,     1,     2,
 i_parent_start                      = 1,     31,    30,
 j_parent_start                      = 1,     17,    30,
 parent_grid_ratio                   = 1,     3,     3,
 parent_time_step_ratio              = 1,     3,     3,
 feedback                            = 1,
 smooth_option                       = 0
 sfcp_to_sfcp 			     = .true.
 /

 &physics
 mp_physics                          = 3,     3,     3,
 ra_lw_physics                       = 1,     1,     1,
 ra_sw_physics                       = 1,     1,     1,
 radt                                = 1,    30,    30,
 sf_sfclay_physics                   = 1,     1,     1,
 sf_surface_physics                  = 2,     2,     2,
 bl_pbl_physics                      = 1,     1,     1,
 bldt                                = 0,     0,     0,
 cu_physics                          = 3,     1,     0,
 cudt                                = 5,     5,     5,
 isfflx                              = 1,
 ifsnow                              = 0,
 icloud                              = 1,
 surface_input_source                = 1,
 num_soil_layers                     = 4,
 sf_urban_physics                    = 0,     0,     0,
 maxiens                             = 1,
 maxens                              = 3,
 maxens2                             = 3,
 maxens3                             = 16,
 ensdim                              = 144,
 cu_diag			     = 1,
 /

 &fdda
 /

 &dynamics
 w_damping                           = 1,
 diff_opt                            = 1,
 km_opt                              = 4,
 diff_6th_opt                        = 0,      0,      0,
 diff_6th_factor                     = 0.12,   0.12,   0.12,
 base_temp                           = 290.
 damp_opt                            = 0,
 zdamp                               = 5000.,  5000.,  5000.,
 dampcoef                            = 0.2,    0.2,    0.2
 khdif                               = 0,      0,      0,
 kvdif                               = 0,      0,      0,
 non_hydrostatic                     = .true., .true., .true.,
 moist_adv_opt                       = 1,      1,      1,     
 scalar_adv_opt                      = 1,      1,      1,     
 /

 &bdy_control
 spec_bdy_width                      = 5,
 spec_zone                           = 1,
 relax_zone                          = 4,
 specified                           = .true., .false.,.false.,
 nested                              = .false., .true., .true.,
 /

 &grib2
 /

 &chem
 kemit                               = 19,
 chem_opt                            = 13,        1,
 bioemdt                             = 30,       30,
 photdt                              = 30,       30,
 chemdt                              = 2.,       2.,
 io_style_emissions                  = 1,
 emiss_inpt_opt                      = 1,        1,
 emiss_opt                           = 3,        3,
 chem_in_opt                         = 0,        0,
 phot_opt                            = 1,        1,
 gas_drydep_opt                      = 1,        1,
 aer_drydep_opt                      = 1,        1,
 bio_emiss_opt                       = 1,        1,
 dust_opt                            = 0,
 dmsemis_opt                         = 0,
 seas_opt                            = 0,
 gas_bc_opt                          = 1,        1,
 gas_ic_opt                          = 1,        1,
 aer_bc_opt                          = 1,        1,
 aer_ic_opt                          = 1,        1,
 gaschem_onoff                       = 1,        1,
 aerchem_onoff                       = 1,        1,
 wetscav_onoff                       = 0,        0,
 cldchem_onoff                       = 0,        0,
 vertmix_onoff                       = 1,        1,
 chem_conv_tr                        = 1,        1,
 biomass_burn_opt                    = 0,        0,
 plumerisefire_frq                   = 30,       30,
 aer_ra_feedback                     = 0,        0,
 have_bcs_chem                       = .false., .false.,
 /

 &namelist_quilt
 nio_tasks_per_group = 0,
 nio_groups = 1,
 /
End_Of_Namelist

echo Running WRF preprocessor...

# Run the real- case preprocessor
cd $wrfRegularDir
ulimit -s unlimited
./real.exe

echo Rinning WRF...

# Run the WRF itself
mpiexec -np 2 ./wrf.exe	
mv wrfout* /home/dmitry/data/OUTPUT/CHEM/

# Do the housekeeping

# Files, left from WPS
rm -f $wpsDir/FILE:*
rm -f $wpsDir/met_em.*
rm -f $wpsDir/geo_em.*

# Files, left from real.exe and wrf.exe
#rm -f $wrfRegularDir/rsl.out.*
#rm -f $wrfRegularDir/rsl.error.*
rm -f $wrfRegularDir/met_em.*
rm -f $wrfRegularDir/geo_em.*
echo Finished at `date`
